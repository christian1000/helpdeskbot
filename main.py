# -*- coding: utf-8 -*-

import os.path
import telegram
import redis
import gettext
import configparser
from cep import api as cepapi

from functools import wraps
from telegram.ext import Updater, CommandHandler, MessageHandler,\
                         RegexHandler, Filters

# Definindo a configuração do Bot
# Configuring bot
# TODO - Refactoring - uma classe para esse config evitaria imports \
#        na classe principal
# TODO - PP - Toda essa parte deviria ser uma Facade, comecando aqui ( Facade FConfig )
config = configparser.ConfigParser()
config.read_file(open('config.ini'))

# Conectando a API do Telegram
# o Updater retorna as informações e o Dispatcher recebe os comandos
# Connecting to Telegram API
# Updater retrieves information and dispatcher connects commands
# TODO - PP - Facade FConfig
updater = Updater(token=config['DEFAULT']['token'])
dispatcher = updater.dispatcher

# Configura as traduções criando o objeto lang_pt
# Config the translations
# TODO - PP - Facade FConfig
lang_pt = gettext.translation("pt_BR", localedir="locale", languages=["pt_BR"])
def _(msg): return msg

# Connecting to Redis db
db = redis.StrictRedis(host=config['DB']['host'],
                       port=config['DB']['port'],
                       db=config['DB']['db'])



# TODO Refactoring: Criar uma classe para essa função e movê-la para lá
def user_language(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        lang = db.get(str(update.message.chat_id))

        global _

        if lang == b"pt_BR":
            # If language is pt_BR, translates
            _ = lang_pt.gettext
        else:
            # If not, leaves as en_US
            def _(msg): return msg

        result = func(bot, update, *args, **kwargs)
        return result
    return wrapped


# TODO - PP  - Vide o solicitado em Composite Hnd
@user_language
def start(bot, update):
    """
        Exibe uma mensagem de boas vindas e informa os possíveis comandos.
        Shows an welcome message and help info about the available commands.
    """

    # TODO - PP - Um decorator aqui evitaria essa linha constante para debug,\
    #             crie o decorator my_debug
    print("Chamada a funcao start")


    me = bot.get_me()

    # Welcome message
    msg = _("Hello!\n")
    msg += _("I'm {0} and I came here to help you.\n").format(me.first_name)
    msg += _("What would you like to do?\n\n")
    msg += _("/support - Opens a new support ticket\n")
    msg += _("/settings - Settings of your account\n\n")
    msg += _("/cep_proximo - Verify nearest CEP from your localization")

    # Commands menu
    # Menu de comandos
    main_menu_keyboard = [[telegram.KeyboardButton('/support')],
                          [telegram.KeyboardButton('/settings')]]
    reply_kb_markup = telegram.ReplyKeyboardMarkup(main_menu_keyboard,
                                                   resize_keyboard=True,
                                                   one_time_keyboard=True)

    # Send the message with menu
    # Enviar a mensagem
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg,
                     reply_markup=reply_kb_markup)

# TODO - PP - Vide o solicitado em Composite Hnd
@user_language
def support(bot, update):
    """
        Envia a mensagem de suporte. Algo como "Como posso ajudá-lo?".
        Sends the support message. Some kind of "How can I help you?".
    """


    # TODO - PP - Um decorator aqui evitaria essa linha constante para debug,\
    #             crie o decorator my_debug
    print("Chamada a funcao support")

    bot.send_message(chat_id=update.message.chat_id,
                     text=_("Please, tell me what you need support with :)"))


@user_language
def support_message(bot, update):
    """
        Recebe uma mensagem do usuário.

        Se a mensagem é uma resposta para o usuário, o bot fala com o mesmo
        enviando o conteúdo dessa mensagem. Se a mensagem é uma requisição do
        usuário, o bot envia a mensagem para o grupo de suporte.

        ----------------------------------------------------------------------

        Receives a message from the user.

        If the message is a reply to the user, the bot speaks with the user
        sending the message content. If the message is a request from the user,
        the bot forwards the message to the support group.
    """
    if update.message.reply_to_message and \
       update.message.reply_to_message.forward_from:
        # Se é uma resposta para o usuário, o bot responde o mesmo
        # If it is a reply to the user, the bot replies the user
        bot.send_message(chat_id=update.message.reply_to_message
                         .forward_from.id,
                         text=update.message.text)
    else:
        # Caso seja uma requisição do usuario, o bot encaminha a mensagem
        # para o grupo
        # If it is a request from the user, the bot forwards the message
        # to the group
        bot.forward_message(chat_id=int(config['DEFAULT']['support_chat_id']),
                            from_chat_id=update.message.chat_id,
                            message_id=update.message.message_id)
        bot.send_message(chat_id=update.message.chat_id,
                         text=_("Give me some time to think. Soon I will return to you with an answer."))


# TODO - PP - Vide o solicitado em Composite Hnd
@user_language
def settings(bot, update):
    """
        Exibe um teclado modificado para configurar a línguagem.
        Configure the messages language using a custom keyboard.
    """

    # TODO - PP - Um decorator aqui evitaria essa linha constante para debug,\
    #             crie o decorator my_debug
    print("Chamada a funcao settings")


    # Languages message
    msg = _("Please, choose a language:\n")
    msg += "en_US - English (US)\n"
    msg += "pt_BR - Português (Brasil)\n"

    # Languages menu
    languages_keyboard = [
        [telegram.KeyboardButton('en_US - English (US)')],
        [telegram.KeyboardButton('pt_BR - Português (Brasil)')]
    ]
    reply_kb_markup = telegram.ReplyKeyboardMarkup(languages_keyboard,
                                                   resize_keyboard=True,
                                                   one_time_keyboard=True)

    # Sends message with languages menu
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg,
                     reply_markup=reply_kb_markup)


@user_language
def kb_settings_select(bot, update, groups):
    """
        Atualiza a linguagem escolhida pelo usuário com base em sua escolha.
        Updates the user's language based on it's choice.
    """
    chat_id = update.message.chat_id
    language = groups[0]

    # Available languages
    languages = {"pt_BR": "Português (Brasil)",
                 "en_US": "English (US)"}

    # If the language choice matches the expression AND is a valid choice
    if language in languages.keys():
        # Sets the user's language
        db.set(str(chat_id), language)
        bot.send_message(chat_id=chat_id,
                         text=_("Language updated to {0}")
                         .format(languages[language]))
    else:
        # If it is not a valid choice, sends an warning
        bot.send_message(chat_id=chat_id,
                         text=_("Unknown language! :("))


# TODO - PP - Vide o solicitado em Composite Hnd
@user_language
def cep_proximo(bot, update):
    """
      Usando a API CEP informa qual o CEP mais proximo do local do usuário
    """


    # TODO - PP - Um decorator aqui evitaria essa linha constante para debug,\
    #             crie o decorator my_debug
    print("Chamada a funcao cep_proximo")

    chat_id = update.message.chat_id
    location_keyboard = telegram.KeyboardButton(text=_("Send localization!"), request_location=True)
    custom_keyboard = [[ location_keyboard ]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
    bot.send_message(chat_id=chat_id,
                     text=_("Would you mind sharing your location with me?"),
                     reply_markup=reply_markup)


@user_language
def location(bot, update):
    """
        Captura o envio da localização e trata
    """

    # TODO - PP - Um decorator aqui evitaria essa linha constante para debug,\
    #             crie o decorator my_debug
    print("Chamada a funcao location")

    chat_id = update.message.chat_id
    loc = update.message.location
    # TODO - PP - Um singleton para evitar que toda vez seja criado o objeto c
    c = cepapi.CEPAPI(token=config['DEFAULT']['cep_token'])
    js = c.getCEPMaisProximo(lng=loc['longitude'],lat=loc['latitude'])
    bot.send_message(chat_id=chat_id,text=js)

# TODO - PP - Vide o solicitado em Composite Hnd
@user_language
def unknown(bot, update):
    """
        Comando padrão para quando o usuário enviar um comando desconhecido.
        Placeholder command when the user sends an unknown command.
    """


    # TODO - PP - Um decorator aqui evitaria essa linha constante para debug,\
    #             crie o decorator my_debug
    print("Chamada a funcao unknow")

    msg = _("Sorry, I don't know what you're asking for.")
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg)

# TODO - PP - Vide o solicitado em Composite Hnd
# criando os handlers
# creating handlers
start_handler = CommandHandler('start', start)
support_handler = CommandHandler('support', support)
support_msg_handler = MessageHandler(Filters.text, support_message)
settings_handler = CommandHandler('settings', settings)
cep_proximo_handler = CommandHandler('cep_proximo', cep_proximo)
get_language_handler = RegexHandler('^([a-z]{2}_[A-Z]{2}) - .*',
                                    kb_settings_select,
                                    pass_groups=True)
help_handler = CommandHandler('help', start)
unknown_handler = MessageHandler(Filters.command, unknown)

# adding handlers
# TODO - PP - Composite Hnd - Implemente um Composite para esses Handlers
dispatcher.add_handler(start_handler)
dispatcher.add_handler(support_handler)
dispatcher.add_handler(settings_handler)
dispatcher.add_handler(get_language_handler)
dispatcher.add_handler(help_handler)
dispatcher.add_handler(cep_proximo_handler)


dispatcher.add_handler(unknown_handler)

# Este não precisa ir no Command Hnd
dispatcher.add_handler(MessageHandler(Filters.location, location))

# Message handler must be the last one
# ATENCAO - O Message handler ( support_msg_handler ) deve ser o ultimo a ser adicionado
dispatcher.add_handler(support_msg_handler)

# para executar esse programa:
# to run this program:
# updater.start_polling()
# to stop it:
# updater.stop()
