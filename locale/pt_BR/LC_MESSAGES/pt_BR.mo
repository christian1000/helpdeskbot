��          �            h  8   i  &   �  &   �  E   �     6  %   >     d     |  -   �     �  +   �            -   7  �  e  K   �  *   <  *   g  H   �     �  &   �     	  "   '  8   J     �  0   �     �      �  /                                            
       	                                /cep_proximo - Verify nearest CEP from your localization /settings - Settings of your account

 /support - Opens a new support ticket
 Give me some time to think. Soon I will return to you with an answer. Hello!
 I'm {0} and I came here to help you.
 Language updated to {0} Please, choose a language:
 Please, tell me what you need support with :) Send localization! Sorry, I don't know what you're asking for. Unknown language! :( What would you like to do?

 Would you mind sharing your location with me? Project-Id-Version: helpdeskbot
PO-Revision-Date: 2018-11-25 01:57-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: main.py
 /cep_proximo - Verifica qual o CEP mais próximo da sua localização atual /settings - Configurações da sua conta

 /support - Abre um novo ticket de suporte
 Me dê um tempo para pensar. Em breve eu irei retornar com uma resposta. Olá!
 Eu sou {0} e vim aqui para te ajudar.
 Linguagem atualizada para {0} Por favor, escolha uma linguagem:
 Por favor, me diga com o que você precisa de suporte :) Enviar localização! Desculpe, eu não sei o que você está pedindo. Linguagem desconhecida! :( O que você gostaria de fazer?

 Você deseja informar sua localização comigo? 