# HelpDesk Bot
> A Python Telegram Bot that works as a helpdesk software.
> Projeto Original https://github.com/juliarizza/helpdeskbot

# Objetivo
  Permitir treino de Padroes de Projeto

# O que o bot faz?
Quando um client envia uma mensagem para o bot do suporte, ele redireciona a mesma para o grupo da empresa e ela pode ser respondida. E ela sendo respondida a partir do grupo é reencaminhda ( pelo bot ) para o cliente.

## Fluxo

1. O cliente conversa com o bot
![The client talks to the bot screenshot](screenshots/screenshot1.png)

2. O grupo da empresa recebe a mensagem e a responde.
![The company receives the message and replies it screenshot](screenshots/screenshot2.png)

3. O cliente recebe a resposta e o processo continua.
![The client receives the answer screenshot](screenshots/screenshot3.png)

# Instalação ( baseado no Ubuntu 18.04 )

1. Faça o **![fork](https://gitlab.com/bemanuel/helpdeskbot/forks/new)** do projeto para sua conta

2. Instale os requisitos

```
    ~$ sudo apt update
    ~$ sudo apt install python3-pip build-essential redis-server git vim python3-distutils-extra
    ~$ sudo /etc/init.d/redis-server start
    ~$ git clone <<url_do_seu_projeto_clonado>>
    ~$ cd helpdeskbot
    ~/helpdeskbot$ pipenv install
    ~/helpdeskbot$ pipenv update
    ~/helpdeskbot$ pipenv shell
```

3. No arquivo config.ini registre o token do Bot e do CEP Aberto no arquivo `config.ini`

   **PS.**: Para gerar o token acesse o site ![CEP Aberto](https://goo.gl/QoXPy6) e crie **gratuitamente** um registro, após isso clique no menu ![API](http://cepaberto.com/api_key) e na sessão **Token de Acesso** você terá o hash do token.

 ![Token CEP Aberto](screenshots/screenshot_cep_api.png)

4. Abra o shell Python e execute:

```
    ~/helpdeskbot$ pipenv shell
    (helpdeskbot) ~/helpdeskbot$ pipenv lock
    (helpdeskbot) ~/helpdeskbot$ python3
    >>> from main import updater
    >>> updater.start_polling()
```

Enquanto quiser que o bot fique rodando deixe esse shell aberto. Quando quiser parar o bot, digite:

```
    updater.stop()
```

# Exercício

  Todas as questões estão marcadas com a TAG TODO. Para listá-las via linha de comando use o grep:
  ```
    ~/helpdeskbot$ grep -ir TODO .
  ```
 ![Todos](screenshots/screenshot_todo.png)

# Direitos do código base
Copyright (C) 2016 Júlia Rizza & licensed under MIT License
